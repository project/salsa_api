<?php
/**
 * @file
 * Contains \Drupal\salsa_api\SalsaQueryException.
 */

namespace Drupal\salsa_api;

/**
 * Exception thrown in case an error is returned by Salsa.
 */
class SalsaQueryException extends \Exception {

}
