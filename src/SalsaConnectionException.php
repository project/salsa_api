<?php
/**
 * @file
 * Contains \Drupal\salsa_api\SalsaConnectionException.
 */

namespace Drupal\salsa_api;

/**
 * Exception thrown when the initial connection or authentication failed.
 */
class SalsaConnectionException extends SalsaQueryException {

}
